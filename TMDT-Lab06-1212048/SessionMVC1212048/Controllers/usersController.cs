﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SessionMVC1212048.Models;

namespace SessionMVC1212048.Controllers
{
    public class usersController : Controller
    {
        //
        // GET: /users/
        //private SinhVien1212048 sv = new SinhVien1212048();
        QuanLySinhVienEntities db = new QuanLySinhVienEntities();
        public ActionResult Index()
        {
            
            return View(db.Employees.ToList());
        }
        public ActionResult loadUserByMSSV(string mssv)
        {
            Employee QS = new EmployeeDAO().find(mssv);
            return View(QS);
        }
        
    }
}
