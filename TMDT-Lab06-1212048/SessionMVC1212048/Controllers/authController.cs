﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SessionMVC1212048.Models;
namespace SessionMVC1212048.Controllers
{
    public class authController : Controller
    {
        //
        // GET: /auth/
        public ActionResult login()
        {
            return View();
        }
        public ActionResult check(AccountViewModel1212048 avm)
        {
            if(avm.account.UserName.Equals("1212048") && avm.account.PassWord.Equals("TMDT2015"))
            {
                Session["Username"] = avm.account.UserName;
                return View("Index");
            }
            else{
                ViewBag.Error = "Login Fail";
            return View("login");
            }
        }
            #region All Views' names
        enum AllViewsNames
        {
            RazorIndex,
            ASPXIndex
        }

        #endregion

        static AllViewsNames currentViewEnum = AllViewsNames.ASPXIndex;
        // Current view name;
        string strCurrentView = currentViewEnum == AllViewsNames.RazorIndex ? "Index" : "TestPage";

        public ActionResult Index()
        {
            return View(strCurrentView);
        }

        /// <summary>
        /// ActionResult for ordinary session(HttpContext).
        /// </summary>
        /// <param name="sessionValue"></param>
        /// <returns></returns>
        public ActionResult SaveSession(string sessionValue)
        {
            try
            {
                System.Web.HttpContext.Current.Session["sessionString"] = sessionValue;
                return RedirectToAction("LoadSession");
            }
            catch (InvalidOperationException)
            {
                return View(strCurrentView);
            }
        }

        /// <summary>
        /// Load session data and redirect to TestPage.
        /// </summary>
        /// <returns></returns>
        public ActionResult LoadSession()
        {
            LoadSessionObject();
            return View(strCurrentView);
        }

        /// <summary>
        /// ActionResult for Extension method.
        /// </summary>
        /// <param name="sessionValue"></param>
        /// <returns></returns>
        public ActionResult SaveSessionByExtensions(string sessionValue)
        {
            try
            {
                Session.SetDataToSession<string>("key1", sessionValue);
                return RedirectToAction("LoadSession");
            }
            catch (InvalidOperationException)
            {
                return View(strCurrentView);
            }
        }

        /// <summary>
        /// Store the session value to ViewData.
        /// </summary>
        private void LoadSessionObject()
        {
            // Load session from HttpContext.
            ViewData["sessionString"] = System.Web.HttpContext.Current.Session["sessionString"] as String;

            // Load session by Extension method.
            string value = Session.GetDataFromSession<string>("key1");
            ViewData["sessionStringByExtensions"] = value;
        }
        }
    }
}
