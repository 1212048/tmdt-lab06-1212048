﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SessionMVC1212048.Models;
namespace SessionMVC1212048.Models
{
    public class EmployeeDAO
    {
        public Employee find(string Ma)
        {
            QuanLySinhVienEntities data = new QuanLySinhVienEntities();
            Employee old = (from p in data.Employees
                             where p.ID == Ma
                             select p).Single<Employee>();
            return old;
        }
    }
}