USE [QuanLySinhVien]
GO

/****** Object:  Table [dbo].[Employee]    Script Date: 10/12/2015 03:27:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Employee](
	[ID] [nvarchar](10) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[_address] [nvarchar](100) NULL,
	[email] [nvarchar](50) NULL,
	[phone] [nvarchar](15) NULL,
	[salary] [nvarchar](50) NULL,
	[departmentid] [nvarchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_Employee_Department] FOREIGN KEY([departmentid])
REFERENCES [dbo].[Department] ([ID])
GO

ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_Employee_Department]
GO


